-- Drop all sequences
DROP SEQUENCE seq_persons;
DROP SEQUENCE seq_users;
DROP SEQUENCE seq_addresses;
DROP SEQUENCE seq_address_types;
DROP SEQUENCE seq_phones;
DROP SEQUENCE seq_phone_types;
DROP SEQUENCE seq_states;

--Drop all tables
DROP TABLE persons_phones_xref;
DROP TABLE persons_addresses_xref;
DROP TABLE users;
DROP TABLE persons;
DROP TABLE phone_types;
DROP TABLE address_types;
DROP TABLE phones;
DROP TABLE addresses;
DROP TABLE states;

-- Create person table
CREATE TABLE persons (
    id INT PRIMARY KEY,
    first_name VARCHAR2(40) NOT NULL,
    last_name VARCHAR2(40) NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Create states table
CREATE TABLE states (
    id INT PRIMARY KEY,
    state_code CHAR(2) NOT NULL UNIQUE,
    state_name VARCHAR2(128) NOT NULL UNIQUE    
);

-- Create users table 
CREATE TABLE users (
    id INT PRIMARY KEY,
    user_name VARCHAR2(40) NOT NULL UNIQUE,
    password VARCHAR2(128) NOT NULL,
    -- 1 to 1 (user to person)
    person_id INT UNIQUE REFERENCES persons(id),
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Address table
-- 1 to 1 address to type
CREATE TABLE addresses (
    id INT PRIMARY KEY,
    street_1 VARCHAR2(128) NOT NULL,
    street_2 VARCHAR2(128),
    city VARCHAR2(60) NOT NULL,
    state_id INT NOT NULL REFERENCES states(id),
    zip VARCHAR2(5) NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Phone table (not sure if it should be this broken down)
CREATE TABLE phones (
    id INT PRIMARY KEY,
    phone_number CHAR(10) NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Address type table
CREATE TABLE address_types (
    id INT PRIMARY KEY,
    address_type VARCHAR2(24)
);

-- Phone type table
CREATE TABLE phone_types (
    id INT PRIMARY KEY,
    phone_type VARCHAR2(24)
);

-- Cross Refernce Table (persons, addresses, address_types)
CREATE TABLE persons_addresses_xref (
    person_id NOT NULL REFERENCES persons(id),
    address_id NOT NULL REFERENCES addresses(id),
    address_type_id NOT NULL REFERENCES address_types(id),
    CONSTRAINT person_address_pk PRIMARY KEY (person_id, address_id, address_type_id)
);

-- Cross Reference Table (persons, phone_numbers, phone_types
CREATE TABLE persons_phones_xref (
    person_id NOT NULL REFERENCES persons(id),
    phone_id NOT NULL REFERENCES phones(id),
    phone_type_id NOT NULL REFERENCES phone_types(id),
    CONSTRAINT person_phone_pk PRIMARY KEY (person_id, phone_id, phone_type_id)
);

-- Create Sequences with values 1 to 100 (100 unique for now)
CREATE SEQUENCE seq_users
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_persons
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_addresses
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_address_types
START WITH 1
INCREMENT BY 1
MAXVALUE 10
MINVALUE 1
CACHE 5
CYCLE
ORDER;

CREATE SEQUENCE seq_phones
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_phone_types
START WITH 1
INCREMENT BY 1
MAXVALUE 10
MINVALUE 1
CACHE 5
CYCLE
ORDER;

CREATE SEQUENCE seq_states
START WITH 1
INCREMENT BY 1
MAXVALUE 60
MINVALUE 1
CACHE 20
CYCLE
ORDER;

/*
-- Insert into states table
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AL', 'Alabama');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AK', 'Alaska');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AZ', 'Arizona');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AR', 'Arkansas');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CA', 'California');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CO', 'Colorado');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CT', 'Connecticut');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'DE', 'Delaware');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'DC', 'District of Columbia');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'FL', 'Florida');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'GA', 'Georgia');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'HI', 'Hawaii');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ID', 'Idaho');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IL', 'Illinois');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IN', 'Indiana');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IA', 'Iowa');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'KS', 'Kansas');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'KY', 'Kentucky');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'LA', 'Louisiana');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ME', 'Maine');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MD', 'Maryland');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MA', 'Massachusetts');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MI', 'Michigan');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MN', 'Minnesota');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MS', 'Mississippi');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MO', 'Missouri');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MT', 'Montana');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NE', 'Nebraska');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NV', 'Nevada');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NH', 'New Hampshire');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NJ', 'New Jersey');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NM', 'New Mexico');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NY', 'New York');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NC', 'North Carolina');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ND', 'North Dakota');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OH', 'Ohio');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OK', 'Oklahoma');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OR', 'Oregon');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'PA', 'Pennsylvania');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'RI', 'Rhode Island');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'SC', 'South Carolina');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'SD', 'South Dakota');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'TN', 'Tennessee');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'TX', 'Texas');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'UT', 'Utah');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'VT', 'Vermont');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'VA', 'Virginia');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WA', 'Washington');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WV', 'West Virginia');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WI', 'Wisconsin');
INSERT INTO states (id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WY', 'Wyoming');

--Populate address types and phone types
INSERT INTO phone_types (id, phone_type) VALUES (seq_phone_types.NEXTVAL, 'Home');
INSERT INTO phone_types (id, phone_type) VALUES (seq_phone_types.NEXTVAL, 'Work');
INSERT INTO phone_types (id, phone_type) VALUES (seq_phone_types.NEXTVAL, 'Cell');
INSERT INTO address_types (id, address_type) VALUES (seq_address_types.NEXTVAL, 'Home');
INSERT INTO address_types (id, address_type) VALUES (seq_address_types.NEXTVAL, 'Work');

-- First user
INSERT INTO persons (id, first_name, last_name) 
VALUES (seq_persons.NEXTVAL, 'David', 'Manni');

INSERT INTO users (id, user_name, password, person_id) 
VALUES (seq_users.NEXTVAL, 'davidmanni', 'password', (SELECT id FROM persons WHERE last_name='Manni' AND first_name='David'));
  
INSERT INTO addresses (id, street_1, city, state_id, zip)
VALUES (seq_addresses.NEXTVAL, '10582 Jane Eyre Dr', 'Orlando', (SELECT id FROM states WHERE state_code='FL'), 32825);

INSERT INTO phones (id, phone_number)
VALUES (seq_phones.NEXTVAL, '1234567890');

INSERT INTO persons_addresses_xref (person_id, address_id, address_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Manni' AND first_name='David'), (SELECT id FROM addresses WHERE street_1='10582 Jane Eyre Dr' AND city='Orlando'),
        (SELECT id FROM address_types WHERE address_type='Home'));

INSERT INTO persons_phones_xref (person_id, phone_id, phone_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Manni' AND first_name='David'), (SELECT id FROM phones WHERE phone_number='1234567890'),
        (SELECT id FROM phone_types WHERE phone_type='Cell'));


-- Second user      
INSERT INTO persons (id, first_name, last_name) 
VALUES (seq_persons.NEXTVAL, 'Jesus', 'Duran');

INSERT INTO users (id, user_name, password, person_id) 
VALUES (seq_users.NEXTVAL, 'jesusduran', 'password', (SELECT id FROM persons WHERE last_name='Duran' AND first_name='Jesus'));
  
INSERT INTO addresses (id, street_1, city, state_id, zip)
VALUES (seq_addresses.NEXTVAL, '123 Oracle St', 'New York City', (SELECT id FROM states WHERE state_code='NY'), 12345);

INSERT INTO phones (id, phone_number)
VALUES (seq_phones.NEXTVAL, '4079130235');

INSERT INTO persons_addresses_xref (person_id, address_id, address_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Duran' AND first_name='Jesus'), (SELECT id FROM addresses WHERE street_1='123 Oracle St' AND city='New York City'),
        (SELECT id FROM address_types WHERE address_type='Work'));

INSERT INTO persons_phones_xref (person_id, phone_id, phone_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Duran' AND first_name='Jesus'), (SELECT id FROM phones WHERE phone_number='4079130235'),
        (SELECT id FROM phone_types WHERE phone_type='Work'));
 
-- Third user      
INSERT INTO persons (id, first_name, last_name) 
VALUES (seq_persons.NEXTVAL, 'Sherlock', 'Holmes');

INSERT INTO users (id, user_name, password, person_id) 
VALUES (seq_users.NEXTVAL, 'sherlock_247', 'password', (SELECT id FROM persons WHERE last_name='Holmes' AND first_name='Sherlock'));
  
INSERT INTO addresses (id, street_1, street_2, city, state_id, zip)
VALUES (seq_addresses.NEXTVAL, '221 Baker St','B', 'London', (SELECT id FROM states WHERE state_code='KY'), 98745);

INSERT INTO phones (id, phone_number)
VALUES (seq_phones.NEXTVAL, '9876543210');

INSERT INTO persons_addresses_xref (person_id, address_id, address_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Holmes' AND first_name='Sherlock'), (SELECT id FROM addresses WHERE street_1='221 Baker St' AND street_2='B' AND city='London'),
        (SELECT id FROM address_types WHERE address_type='Home'));

INSERT INTO persons_phones_xref (person_id, phone_id, phone_type_id)
VALUES ((SELECT id FROM persons WHERE last_name='Holmes' AND first_name='Sherlock'), (SELECT id FROM phones WHERE phone_number='9876543210'),
        (SELECT id FROM phone_types WHERE phone_type='Cell'));
*/