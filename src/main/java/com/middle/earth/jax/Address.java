package com.middle.earth.jax;

import java.util.Date;

public class Address extends JaxBase {
	
	private static final long serialVersionUID = 1L;
	
	int id;		
	String street1;
	String street2;
	String city;
	State stateId;	
	String zip;	
	Date timeCreated;	
	Date timeUpdated;

	public Address(int id, String street1, String street2, String city, State stateId, String zip, Date timeCreated,
			Date timeUpdated) {
		super();
		this.id = id;
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.stateId = stateId;
		this.zip = zip;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String address1) {
		this.street1 = address1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String address2) {
		this.street2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public State getStateId() {
		return stateId;
	}

	public void setStateId(State stateId) {
		this.stateId = stateId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
}
