package com.middle.earth.jax;

import java.util.Date;

public class User extends JaxBase {
	
	private static final long serialVersionUID = 1L;

	int id;
	String userName;
	String password;
	Person person;
	Date timeCreated;
	Date timeUpdated;
	
	public User(int id, String userName, String password, Person person, Date timeCreated, Date timeUpdated) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.person = person;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPersonId(Person person) {
		this.person = person;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
}
