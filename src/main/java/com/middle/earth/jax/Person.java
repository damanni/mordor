package com.middle.earth.jax;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.middle.earth.jax.Address;
import com.middle.earth.jax.AddressType;
import com.middle.earth.jax.Phone;
import com.middle.earth.jax.PhoneType;

public class Person extends JaxBase {
	
	private static final long serialVersionUID = 1L;
	
	int id;
	String firstName;
	String lastName;
	Date timeCreated;
	Date timeUpdated;
	Map<AddressType, Address> address = new HashMap<>();
	Map<PhoneType, Phone> phone = new HashMap<>();

	public Person(int id, String firstName, String lastName, Date timeCreated, Date timeUpdated, Map<AddressType, Address> address,
			Map<PhoneType, Phone> phone) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
		this.address = address;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public Map<AddressType, Address> getAddress() {
		return address;
	}

	public void setAddress(Map<AddressType, Address> address) {
		this.address = address;
	}

	public Map<PhoneType, Phone> getPhone() {
		return phone;
	}

	public void setPhone(Map<PhoneType, Phone> phone) {
		this.phone = phone;
	}
}
