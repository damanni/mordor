package com.middle.earth.jax;

import java.util.Date;

public class Phone extends JaxBase {
	
	private static final long serialVersionUID = 1L;

	int id;
	String phoneNumber;
	Date timeCreated;
	Date timeUpdated;

	public Phone(int id, String phoneNumber, Date timeCreated, Date timeUpdated) {
		super();
		this.id = id;
		this.phoneNumber = phoneNumber;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
}
