package com.middle.earth.jax;

public class PersonPhoneXref extends JaxBase {
	
	private static final long serialVersionUID = 1L;

	Person personId;	
	Phone phoneId;	
	PhoneType phoneTypeId;
	
	public PersonPhoneXref(Person personId, Phone phoneId, PhoneType phoneTypeId) {
		super();
		this.personId = personId;
		this.phoneId = phoneId;
		this.phoneTypeId = phoneTypeId;
	}

	public Person getPersonId() {
		return personId;
	}

	public void setPersonId(Person person) {
		this.personId = person;
	}

	public Phone getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(Phone phoneId) {
		this.phoneId = phoneId;
	}

	public PhoneType getPhoneTypeId() {
		return phoneTypeId;
	}

	public void setPhoneTypeId(PhoneType phoneTypeId) {
		this.phoneTypeId = phoneTypeId;
	}
	
}
