package com.middle.earth;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

/*
 *	@SpringBootApplication annotation offers AutoConfiguration
 */

@SpringBootApplication
@EnableJpaAuditing
@EnableOAuth2Client
@EnableZuulProxy
public class Mordor {
	private static Logger logger = LogManager.getLogger();	
	
    public static void main(String[] args) {
      if(logger.isDebugEnabled())
    	  logger.debug("Starting Mordor Application");
      
      SpringApplication app =
                new SpringApplication(Mordor.class);

      Properties properties = new Properties();
      properties.setProperty("spring.resources.static-locations",
                        "classpath:/static/");
      app.setDefaultProperties(properties);
      app.run(args);
    }
}