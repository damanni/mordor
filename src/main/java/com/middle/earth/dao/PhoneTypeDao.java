package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.PhoneType;

@Repository
@Transactional
public class PhoneTypeDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save in the database.
	 */
	public void create(PhoneType phoneType) {
		entityManager.persist(phoneType);
		return;
	}
	  
	/**
	 * Delete from the database.
	 */
	public void delete(PhoneType phoneType) {
		if (entityManager.contains(phoneType))
			entityManager.remove(phoneType);
		else
			entityManager.remove(entityManager.merge(phoneType));
		return;
	}
	  
	/**
	 * Return all stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<PhoneType> getAllPhoneTypes() {
		return entityManager.createQuery("from PhoneType").getResultList();
	}
	
//	@Autowired
//	SessionFactory sessionFactory;
//	
//	public void save(PhoneType phoneType) {		
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.saveOrUpdate(phoneType);
//	}
//	
//	public void delete(PhoneType phoneType) {
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.delete(phoneType);
//	}

}
