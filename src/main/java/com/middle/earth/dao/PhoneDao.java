package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.Phone;

@Repository
@Transactional
public class PhoneDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save in the database.
	 */
	public void create(Phone phone) {
		entityManager.persist(phone);
		return;
	}
	  
	/**
	 * Delete from the database.
	 */
	public void delete(Phone phone) {
		if (entityManager.contains(phone))
			entityManager.remove(phone);
		else
			entityManager.remove(entityManager.merge(phone));
		return;
	}
	  
	/**
	 * Return all stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<Phone> getAllPhoneNumbers() {
		return entityManager.createQuery("from Phone").getResultList();
	}

}
