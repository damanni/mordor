package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.Person;

@Repository
@Transactional
public class PersonDao {
	
	@PersistenceContext	(type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	public Person getPersonById(int personId) {
		return entityManager.find(Person.class, personId);
	}
	@SuppressWarnings("unchecked")
	public List<Person> getAllPersons() {
		String hql = "FROM Person";
		return (List<Person>) entityManager.createQuery(hql).getResultList();
	}	
	public void addPerson(Person person) {
		entityManager.persist(person);
	}
	public void updatePerson(Person person) {
		Person personcl = getPersonById(person.getId());
		personcl.setFirstName(person.getFirstName());
		personcl.setLastName(person.getLastName());
		entityManager.flush();
	}
	public void deletePerson(int personId) {
		entityManager.remove(getPersonById(personId));
	}
	public boolean personExists(String firstName, String lastName) {
		String hql = "FROM Person pcl WHERE pcl.first_name = ? and pcl.last_name = ?";
		int count = entityManager.createQuery(hql).setParameter(1, firstName)
	              .setParameter(2, lastName).getResultList().size();
		return count > 0 ? true : false;
	}

}
