package com.middle.earth.security.gateway;

import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationException;
import org.springframework.security.providers.AuthenticationProvider;
import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;


@Component
public class SauronAuthenticationProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
        String password = authentication.getCredentials().toString();
         
  
        // use the credentials
        // and authenticate against the third-party system
        return new UsernamePasswordAuthenticationToken(name, password);
    }

	@Override
	public boolean supports(Class authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
   	}

}
