package com.middle.earth.hib;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "phone_types")
public class PhoneType extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phoneType_generator")
	@SequenceGenerator(name="phoneType_generator", sequenceName = "seq_phone_types")
	@Column(name = "id")
	int id;
	
	@Column(name = "phone_type")
	String phoneType;
	
	public PhoneType() {
		super();
	}

	public PhoneType(int id, String phoneType) {
		super();
		this.id = id;
		this.phoneType = phoneType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.PhoneType getJaxPhoneType() {
		com.middle.earth.jax.PhoneType jaxPhoneType = new com.middle.earth.jax.PhoneType(
				getId(), 
				getPhoneType()
			);
		return jaxPhoneType;
	}
	
	@Override
    public String toString() {
        return String.format("phoneType: %s", phoneType);
    }
}
