package com.middle.earth.hib;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"timeCreated", "timeUpdated"})
public class User extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name="user_generator", sequenceName = "seq_users")
	@Column(name = "id")
	int id;
	
	@Column(name = "user_name")
	String userName;
	
	@Column(name = "password")
	String password;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(unique=true, nullable=true, name = "id")
	Person person;
	
	@Column(name = "time_created", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	Date timeCreated;
	
	@Column(name = "time_updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	Date timeUpdated;
	
	public User() {
		super();
	}

	public User(int id, String userName, String password, Person person, Date timeCreated, Date timeUpdated) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.person = person;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPersonId(Person person) {
		this.person = person;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.User getJaxUser() {
		com.middle.earth.jax.User jaxUser = new com.middle.earth.jax.User(
				getId(), 
				getUserName(), 
				getPassword(), 
				getPerson().getJaxPerson(), 
				getTimeCreated(), 
				getTimeUpdated()
			);
		return jaxUser;
	}
}
