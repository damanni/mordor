package com.middle.earth.hib;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "states")
public class State extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "state_generator")
	@SequenceGenerator(name="state_generator", sequenceName = "seq_states")
	@Column(name = "id")
	int id;
	
	@Column(name = "state_code")
	String stateCode;
	
	@Column(name = "state_name")
	String stateName;
	
	public State() {
		super();
	}

	public State(int id, String stateCode, String stateName) {
		super();
		this.id = id;
		this.stateCode = stateCode;
		this.stateName = stateName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}	
	
	@JsonIgnore
	public com.middle.earth.jax.State getJaxState() {
		com.middle.earth.jax.State jaxState = new com.middle.earth.jax.State(
				getId(), 
				getStateCode(), 
				getStateName()
			);
		return jaxState;
	}
}
