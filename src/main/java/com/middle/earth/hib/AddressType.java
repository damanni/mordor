package com.middle.earth.hib;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "address_types")
public class AddressType extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "addressType_generator")
	@SequenceGenerator(name="addressType_generator", sequenceName = "seq_address_types")
	@Column(name = "id")
	int id;
	
	@Column(name = "address_type")
	String addressType;
	
	public AddressType() {
		super();
	}

	public AddressType(int id, String addressType) {
		super();
		this.id = id;
		this.addressType = addressType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.AddressType getJaxAddressType() {
		com.middle.earth.jax.AddressType jaxAddressType = new com.middle.earth.jax.AddressType(
				getId(), 
				getAddressType()
			);
		return jaxAddressType;
	}
	
	@Override
    public String toString() {
        return String.format("addressType: %s", addressType);
    }
}
