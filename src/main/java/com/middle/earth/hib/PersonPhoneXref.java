package com.middle.earth.hib;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "persons_phone_numbers_xref")
@IdClass(PersonPhoneXref.class)
public class PersonPhoneXref extends HibBase {
	
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "person_id")
	Person personId;
	
	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phone_number_id")
	Phone phoneId;
	
	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phone_type_id")
	PhoneType phoneTypeId;

	public PersonPhoneXref() {
		super();
	}

	public PersonPhoneXref(Person personId, Phone phoneId, PhoneType phoneTypeId) {
		super();
		this.personId = personId;
		this.phoneId = phoneId;
		this.phoneTypeId = phoneTypeId;
	}

	public Person getPersonId() {
		return personId;
	}

	public void setPersonId(Person personId) {
		this.personId = personId;
	}

	public Phone getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(Phone phoneId) {
		this.phoneId = phoneId;
	}

	public PhoneType getPhoneTypeId() {
		return phoneTypeId;
	}

	public void setPhoneTypeId(PhoneType phoneTypeId) {
		this.phoneTypeId = phoneTypeId;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.PersonPhoneXref getPersonPhoneXref() {
		com.middle.earth.jax.PersonPhoneXref jaxPersonPhoneXref = new com.middle.earth.jax.PersonPhoneXref(
				getPersonId().getJaxPerson(), 
				getPhoneId().getJaxPhone(),
				getPhoneTypeId().getJaxPhoneType()
			);
		return jaxPersonPhoneXref;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((personId == null) ? 0 : personId.hashCode());
		result = prime * result + ((phoneId == null) ? 0 : phoneId.hashCode());
		result = prime * result + ((phoneTypeId == null) ? 0 : phoneTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonPhoneXref other = (PersonPhoneXref) obj;
		if (personId == null) {
			if (other.personId != null)
				return false;
		} else if (!personId.equals(other.personId))
			return false;
		if (phoneId == null) {
			if (other.phoneId != null)
				return false;
		} else if (!phoneId.equals(other.phoneId))
			return false;
		if (phoneTypeId == null) {
			if (other.phoneTypeId != null)
				return false;
		} else if (!phoneTypeId.equals(other.phoneTypeId))
			return false;
		return true;
	}
	
}