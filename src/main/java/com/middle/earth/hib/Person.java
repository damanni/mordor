package com.middle.earth.hib;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "persons")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"timeCreated", "timeUpdated"})
public class Person extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_generator")
	@SequenceGenerator(name="person_generator", sequenceName = "seq_persons")
	@Column(name="id")
	int id;
	
	@Column(name = "first_name")
	String firstName;
	
	@Column(name = "last_name")
	String lastName;
	
	@Column(name = "time_created", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	Date timeCreated;
	
	@Column(name = "time_updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	Date timeUpdated;
	
	@ElementCollection
    @CollectionTable(name="persons_addresses_xref")
	@MapKeyJoinColumn(name = "address_type_id")
	Map<AddressType, Address> address = new HashMap<>();

	@ElementCollection
    @CollectionTable(name="persons_phones_xref")
	@MapKeyJoinColumn(name = "phone_type_id")
	Map<PhoneType, Phone> phone = new HashMap<>();

	public Person() {
		super();
	}

	public Person(int id, String firstName, String lastName, Date timeCreated, Date timeUpdated, Map<AddressType, Address> address,
			Map<PhoneType, Phone> phone) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
		this.address = address;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	
	public Map<AddressType, Address> getAddress() {
		return address;
	}

	public void setAddress(Map<AddressType, Address> address) {
		this.address = address;
	}

	public Map<PhoneType, Phone> getPhone() {
		return phone;
	}

	public void setPhone(Map<PhoneType, Phone> phone) {
		this.phone = phone;
	}

	@JsonIgnore
	public com.middle.earth.jax.Person getJaxPerson(){
			
		Map<AddressType, Address> hibAddress = getAddress();
		Map<com.middle.earth.jax.AddressType, com.middle.earth.jax.Address> jaxAddress = new HashMap<>();
		for(Map.Entry<AddressType, Address> address : hibAddress.entrySet())
			jaxAddress.put(address.getKey().getJaxAddressType(), address.getValue().getJaxAddress());
		
		Map<PhoneType, Phone> hibPhone = getPhone();
		Map<com.middle.earth.jax.PhoneType, com.middle.earth.jax.Phone> jaxPhone = new HashMap<>();
		for(Map.Entry<PhoneType, Phone> phone : hibPhone.entrySet())
			jaxPhone.put(phone.getKey().getJaxPhoneType(), phone.getValue().getJaxPhone());
		
		com.middle.earth.jax.Person jaxPerson = new com.middle.earth.jax.Person(
				getId(), 
				getFirstName(), 
				getLastName(),  
				getTimeCreated(), 
				getTimeUpdated(),
				jaxAddress,
				jaxPhone
			);
		return jaxPerson;
	}
}
