package com.middle.earth.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.middle.earth.dao.UserDao;
import com.middle.earth.jax.User;


@Service
public class PeopleServiceImpl implements IPeopleService {
	
	@SuppressWarnings("unused")
	private static Logger log = LogManager.getLogger();
	
	@Autowired
	private UserDao userDao;
		
	public String PeopleMessage() {
		return "This is the Login Page";
	}

	public List<User> getAllUsers() {		
		List<User> jaxUsers = new ArrayList<>();
		List<com.middle.earth.hib.User> hibList = userDao.getAllUsers();
		
		for(com.middle.earth.hib.User hibUsers : hibList)
			jaxUsers.add(hibUsers.getJaxUser());		
		return jaxUsers;
	}

	public User getUserById(int userId) {
		com.middle.earth.hib.User obj = userDao.getUserById(userId);
		User jaxUser = obj.getJaxUser();
		return jaxUser;
	}
	
	public User getUserByUserName(String userName) {
		com.middle.earth.hib.User user = userDao.getUserByUserName(userName);
		User jaxUser = user.getJaxUser();
		return jaxUser;
	}

	public synchronized boolean addUser(User user) {
		com.middle.earth.hib.User hibUser = new com.middle.earth.hib.User();
		hibUser.setId(user.getId());
		hibUser.setUserName(user.getUserName());
		hibUser.setPassword(user.getPassword());
		
		if (userDao.userExists(hibUser.getUserName())) {
            return false;
        } else {
        	userDao.addUser(hibUser);
            return true;
        }		
	}

	public void updateUser(User user) {
		com.middle.earth.hib.User hibUser = new com.middle.earth.hib.User();
		hibUser.setId(user.getId());
		hibUser.setUserName(user.getUserName());
		hibUser.setPassword(user.getPassword());
		userDao.updateUser(hibUser);		
	}

	public void deleteUser(int userId) {
		userDao.deleteUser(userId);		
	}

}