package com.middle.earth.services;

import java.util.List;
import com.middle.earth.jax.User;

public interface IPeopleService {
	String PeopleMessage();
	List<User> getAllUsers();
	User getUserById(int userId);
	User getUserByUserName(String userName);
	boolean addUser(User user);
	void updateUser(User user);
	void deleteUser(int userId);

}
